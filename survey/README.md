---
title: "Debconf20 and COVID-19: survey results"
---

## Introduction

Back when the COVID-19 outbreak became a global issue, we considered
whether to cancel or postpone DebConf20. At the time we had very little
evidence about what the future might look like, so we decided to come to
a decision by the beginning of June.

To that effect, we asked the Debian Community to participate in a
[survey](https://lists.debian.org/debconf-announce/2020/05/msg00002.html).

## Survey Questions

* Q1: "Prior to the pandemic, were you planning to attend DebConf20 in
  Haifa?";
* Q2: "In your opinion, how should DebConf20 be held?"
* Q3: "Assume we could organize DebConf20 as an in-person conference on
  the original dates (August 23rd to 29th with DebCamp the week before), and
  that governments and airlines make it possible for you to come. How likely
  would it be that you come to DebConf20? Please rate from 1 to 5, with 1
  meaning "Not likely at all", and 5 meaning "Very likely"
* Q4: "If DebConf were held entirely online, do you plan to attend /
* Q5: "Any further comments you want to share with us?"

There was also a free-form text field for comments at the end of the survey,
that will still be worked on.

## Results

We had 419 complete answers to the survey. Quantitative results are presented
in two sub-sections: the first presents data based on all respondents, and the
second based only on the respondents who answered they were previously planning
to travel to Haifa for DebConf20. A third subsection summarizes the free-form
comments.

In the results below, percentages can add up to more than 100% due to rounding.

### Overall results

Q1: _"Prior to the pandemic, were you planning to attend DebConf20 in Haifa?"_

Response|Percentage
-------:|---------:
No | 67%
Yes | 33%

![Chart for Q1](all/01-before.png)

Q2: _"In your opinion, how should DebConf20 be held?"_

Response|Percentage
-------:|---------:
As an in-person conference with video streaming (as usual) | 11%
As an online-only conference                               | 75%
Cancelled (no DebConf in 2020)                             | 14%

![Chart for Q2](all/02-whattodo.png)

Q3: _"Assume we could organize DebConf20 as an in-person conference on the original dates (August 23rd to 29th with DebCamp the week before), and that governments and airlines make it possible for you to come. How likely would it be that you come to DebConf20? Please rate from 1 to 5, with 1 meaning "Not likely at all", and 5 meaning "Very likely"_

Response|Count|Percentage
-------:|----:|---------:
1       | 311 | 74% |
2       |  49 | 12% |
3       |  21 |  5% |
4       |  15 |  4% |
5       |  23 |  5% |


![Chart for Q3](all/03-coming.png)

Q4: _"If DebConf were held entirely online, do you plan to attend / speak?"_

Response|Percentage
-------:|----------:
No |  23%
Yes | 77%

![Chart for Q4](all/04-online.png)

### Results for the people who were previously planning to go Haifa for DebConf20

Most of the respondents did not plan to attend DebConf20 presentially in Haifa.
This section presents the results for only those who were going to be there, had
COVID-19 not happen so that our perception is not skewed by the opinions of
those who were not going to travel to Haifa anyway.

Q2: _"In your opinion, how should DebConf20 be held?"_

Response|Percentage
-------:|---------:
As an in-person conference with video streaming (as usual) | 24%
As an online-only conference | 59%
Cancelled (no DebConf in 2020) | 18%

![Chart for Q2 for people previously planning to travel](planning/02-whattodo.png)

Q3: _"Assume we could organize DebConf20 as an in-person conference on the original dates (August 23rd to 29th with DebCamp the week before), and that governments and airlines make it possible for you to come. How likely would it be that you come to DebConf20? Please rate from 1 to 5, with 1 meaning "Not likely at all", and 5 meaning "Very likely"_

Response|Count|Percentage
-------:|----:|----------:
1       | 61  | 44%
2       | 27  | 19%
3       | 14  | 10%
4       | 15  | 10%
5       | 23  | 16%


![Chart for Q3 for people previously planning to travel](planning/03-coming.png)

Q4: _"If DebConf were held entirely online, do you plan to attend / speak?"_

Response|Percentage
-------:|---------:
No | 21%
Yes | 79%

![Chart for Q4 for people previously planning to travel](planning/04-online.png)

### A summary of the comments 



## Analysis

Support for having an in-person component of DebConf20 among all the
respondents is 11%, while it's 24% among the people who were previously
planning to go.

Of the people previously planning to travel to Haifa, 26% think it's likely (4
and 5 in the scale) they would come.

77% of the respondents would be willing to participate in an entirely online
DebConf. Among the people previously planning to travel to Haifa, that number
is even higher: 79%.

## Raw data

[A CSV file](results.csv) with the anonymized survey results is available if
others want to provide a more sophisticated data analysis. It has the following
columns, corresponding to the survey questions Q1-Q4:

CSV field name | Question
---------------|---------
before | Q1
whattodo | Q2
coming | Q3
online | Q4

The full results file is not available publically as it also includes data that
could be potentially used to identify respondents, such as IP addresses and
form submission dates.
